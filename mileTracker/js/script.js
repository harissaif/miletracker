$(document).one('pageinit', function(){

	//Display	runs
	showRuns();
	
	//add handler
	$('#submitAdd').on('tap', addRun);
	/*
		Show all runs on homepage
	*/
	
	function showRuns(){
		//get runs object
		var runs = getRunsObject();
		
		//check if empty
		if(runs != '' && runs != null){
			for(var i = 0;i < runs.length;i++){
				$('#stats').append('<li class="ui-body-inherit ui-li-static"><strong>Date:</strong>'+runs[i]["date"]+
				' <br><strong>Distance: </strong>'+runs[i]["miles"]+'m<div class="controls">' +
				'<a href="#edit">Edit</a> | <a href="#">Delete</a></li>');
			}
			
			$('#home').bind('pageinit', function(){
				$('#stats').listview('refresh');
			});
		}
	}
	/*
		Add a run
	*/
	function addRun(){
		//Get form values
		
		var miles = $('#addMiles').val();
		var date = $('#addDate').val();
		
		//create run object
		var run = {
			date: date,
			miles: parseFloat(miles)
		};
		
		var runs = getRunsObject();
		
		//Add run to runs array
		runs.push(run);
		alert('Runs Added');
		
		// Set stringfied object to localstorage
		
		localStorage.setItem('runs', JSON.stringify(runs));
		
		// redirect to the index page
		window.location.href="index.html";
		return false;
	}	
	/*
		Get the run object
	*/
	function getRunsObject(){
		//Set runs array
		var runs = new Array();
		//get current runs from local storage
		var currentRuns = localStorage.getItem('runs');
		
		//check local storage
		if(currentRuns != null){
			//set the runs
			var runs = JSON.parse(currentRuns);
		}
		
		//Return runs object
		return runs.sort(function(a,b){return new Date(b.date) - new Date(a.date)});
	}
});